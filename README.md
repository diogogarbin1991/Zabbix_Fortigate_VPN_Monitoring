# Zabbix Fortigate VPN Monitoring



# Installation

Clone the repository, Put the .mibs file into '/usr/local/share/snmp/mibs' if doesn't exists create it

edit your snmp.conf file  in /etc/snmp/ adding the following line:

mibdirs +/usr/local/share/snmp/mibs

Now you must import the .xml file into your zabbix interface and add it to your fortigate host

# Features

Low Level Discovery Discovery

-Item Prototype - Brings tunnel vpn status

-Trigger Prototype - Allert when tunnel is down

# Conclusion

this template already run under fortigate 300D

For any problem send me a message:

diogogarbinsouza@gmail.com